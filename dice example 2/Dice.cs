﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dice_example_2
{
    class Dice
    {
        int value;
        static Random randomValue = new Random();

        public int Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
        int x;

        public int X
        {
            get { return x; }
            set { x = value; }
        }
        int y;

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        public void Roll()
        {
            
            value = randomValue.Next(1,7);
        }
    }
}
