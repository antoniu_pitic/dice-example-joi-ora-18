﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dice_example_2
{
    public partial class Form1 : Form
    {
        List<Dice> dices = new List<Dice>();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dices = GenerateDices(45);
            ShowDices(dices);
        }

        private List<Dice> GenerateDices(int numberOfDices)
        {
            List<Dice> dices = new List<Dice>();

            for (int i = 0; i < numberOfDices; i++)
            {
                Dice dice = new Dice();
                dice.Y = 100 + (i/7)*60;
                dice.X = 20 + (i%7)*60;
                dice.Roll();
                dices.Add(dice);
            }

            return dices;
        }

        private void ShowDices(List <Dice> dices)
        {
            for (int i = 0; i < dices.Count;i++ )
            {
                Label label = new Label();
                label.AutoSize = true;
                label.BackColor = Color.White;
                label.BorderStyle = BorderStyle.FixedSingle;
                label.Font = new Font("Microsoft Sans Serif", 30F, FontStyle.Regular);
                label.Location = new System.Drawing.Point(dices[i].X, dices[i].Y);
                label.Size = new System.Drawing.Size(128, 48);

                label.Text = dices[i].Value.ToString();

                label.Tag = i.ToString();

                label.Click += new System.EventHandler(ClickPeLabel);
                label.MouseMove += new System.Windows.Forms.MouseEventHandler(ClickPeLabel);
           
                this.Controls.Add(label);
            }
        }
 
        private void ClickPeLabel(object sender, EventArgs e)
        {
            int index = Convert.ToInt32(((Label)sender).Tag);
            dices[index].Roll();
            ((Label)sender).Text = dices[index].Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (var dice in dices)
            {
                dice.Roll();
            }
            HideDices();
            ShowDices(dices);
        }

        private void HideDices()
        {
            foreach (var control in Controls)
            {
                if (control is Label)
                {
                    ((Label)control).Visible = false;
                }
            }
        }

    }
}
